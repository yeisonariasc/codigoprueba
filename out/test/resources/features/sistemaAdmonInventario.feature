@Regresion
Feature: Sistema para el maejo del inventario
  Como usuario
  Quiero acceder al sistema de inventarios
  A traves de la interfaz Web

  @LoginExitoso
  Scenario: Logueo a la aplicación de Administración de Inventarios Exitoso
    Given que Yeison quiere acceder al Administrador de Inventario
    When el ingresa el usuario admin y la clave admin
    Then el ve el texto en pantalla Name

  @LoginFallido
  Scenario Outline: Logueo a la aplicación de Administración de Inventarios Fallido
    Given que Yeison quiere acceder al Administrador de Inventario
    When el ingresa el usuario y la clave
    | usuario   | clave   |
    | <usuario> | <clave> |
    Then el ve el texto de login No Exitoso
    | mensaje   |
    | <mensaje> |
    Examples:
    |  usuario   | clave       | mensaje                   |
    | yarias     | admin       | Username doesnot exists   |
    |            | admin       | Username is required      |
    | yarias     |             | Password is required      |