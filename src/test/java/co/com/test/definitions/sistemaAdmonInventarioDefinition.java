package co.com.test.definitions;

import co.com.test.steps.sistemaAdmonInventarioSteps;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class sistemaAdmonInventarioDefinition {
    @Steps
    sistemaAdmonInventarioSteps sistemaAdmoninventarioSteps;

    @Given("^que Yeison quiere acceder al Administrador de Inventario$")
    public void queYeisonQuiereAccederAlAdministradorDeInventario() throws Exception {
        sistemaAdmoninventarioSteps.abrirUrl();
    }

    @When("^el ingresa el usuario (.*) y la clave (.*)$")
    public void elIngresaElUsuarioAdminYLaClaveAdmin(String usr, String pass) throws Exception {
        sistemaAdmoninventarioSteps.realizarAutenticacion(usr, pass);

    }

    @Then("^el ve el texto en pantalla (.*)$")
    public void elVeElTextoEnPantallaName(String strMensaje) throws Exception {
        sistemaAdmoninventarioSteps.verificarAcceso(strMensaje);
    }


    @When("^el ingresa el usuario y la clave$")
    public void elIngresaElUsuarioYLaClave(DataTable datosLogin) throws Exception {
        sistemaAdmoninventarioSteps.logueoNoExitosoCon(datosLogin);
    }

    @Then("^el ve el texto de login No Exitoso$")
    public void elVeElTextoDeLoginNoExitoso(DataTable datosMensaje) throws Exception {
        sistemaAdmoninventarioSteps.verificaElResultadoCon(datosMensaje);
    }


}
