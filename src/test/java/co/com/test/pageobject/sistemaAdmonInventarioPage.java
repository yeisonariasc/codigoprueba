package co.com.test.pageobject;

import cucumber.api.DataTable;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

@DefaultUrl("http://localhost/xampp/inventory-management-system/index.php")
public class sistemaAdmonInventarioPage extends PageObject {
    @FindBy (id="username")
    private WebElementFacade txtUsuario;

    @FindBy (id="password")
    private WebElementFacade txtClave;

    @FindBy (xpath="//*[@id='loginForm']//div/button")
    private WebElementFacade btnSignin;

    //Label de Acceso Exitoso
    @FindBy (id="productTable")
    private WebElementFacade lblPantalla;

    //Label de Acceso Fallido
    @FindBy (xpath="/html/body/div[1]/div/div/div/div[2]/div")
    private WebElementFacade lblAccesoError;

    public void realizarAutenticacion(String usr, String pass) {
        txtUsuario.sendKeys(usr);
        txtClave.sendKeys(pass);
        btnSignin.click();
    }

    public void verificarAccesoExitoso(String strMensaje) {
        String strPantalla = lblPantalla.getText();
        assertThat(strPantalla, containsString(strMensaje));
    }

    public void verificarMensajeAccesoFallido(DataTable strMensaje) {
        List<List<String>> data = strMensaje.raw();
        String strPantalla = lblAccesoError.getText();
        assertThat(strPantalla, containsString(data.get(1).get(0)));
    }

    public void logueoNoExitoso(DataTable datosLogin) {
        List<List<String>> data = datosLogin.raw();
        txtUsuario.sendKeys(data.get(1).get(0));
        txtClave.sendKeys(data.get(1).get(1));
        btnSignin.click();
    }
}
