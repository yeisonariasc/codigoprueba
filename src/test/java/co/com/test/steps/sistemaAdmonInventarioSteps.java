package co.com.test.steps;

import co.com.test.pageobject.sistemaAdmonInventarioPage;
import cucumber.api.DataTable;
import net.thucydides.core.annotations.Step;

import java.util.List;

public class sistemaAdmonInventarioSteps {
    sistemaAdmonInventarioPage sistemaAdmoninventarioPage;

    @Step
    public void abrirUrl() {
        sistemaAdmoninventarioPage.open();
    }

    @Step
    public void realizarAutenticacion(String usr, String pass) {
        sistemaAdmoninventarioPage.realizarAutenticacion(usr, pass);
    }

    @Step
    public void verificarAcceso(String strMensaje) {
        sistemaAdmoninventarioPage.verificarAccesoExitoso(strMensaje);
    }

//    @Step
//    public void verificarAccesoFallido(String strMensaje) {
//        sistemaAdmoninventarioPage.verificarMensajeAccesoFallido(strMensaje);
//    }

    @Step
    public void logueoNoExitosoCon(DataTable datosLogin) {
        sistemaAdmoninventarioPage.logueoNoExitoso(datosLogin);
    }

    @Step
    public void verificaElResultadoCon(DataTable datosMensaje) {
        sistemaAdmoninventarioPage.verificarMensajeAccesoFallido(datosMensaje);
    }
}
